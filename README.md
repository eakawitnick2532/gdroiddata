This repo contains some open source scripts that collect open source data from open sources on the internet. 
This work could be theoretically be done by a client too, but it is very complex (runtime, networks usage and CPU), so it runs here. 
The collected data will additionally be processed statistically (normalisation etc.) to make it more useful for clients.

The following things are being fetched from other sources:
1. number of stars on gitlab / github / sourceforge
2. amount of time between releases (from dfroiddata)
3. age of the first and latest version (from dfroiddata)

To see the results in plain text, please check the file: metadata/gdroid.json
