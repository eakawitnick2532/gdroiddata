#!/bin/bash
curl https://f-droid.org/repo/index-v1.jar > metadata/index-v1.jar
rm -f metadata/index-v1.json
unzip -j "metadata/index-v1.jar" "index-v1.json" -d "metadata/"

if [ "$1" == "m_github_stars" ]
then
    echo fetching github stars ...
    ./m_github_stars.sh
    exit 0
fi
if [ "$1" == "m_gitlab_stars" ]
then
    echo fetching gitlab stars ...
    ./m_gitlab_stars.sh
    exit 0
fi
if [ "$1" == "meta_magic" ]
then
    echo meta magic ...
    ./meta_magic.sh || exit 1
    exit 0
fi

echo no parameter received. doing nothing

#./m_github_stars.sh
#./m_gitlab_stars.sh
#./meta_magic.sh
