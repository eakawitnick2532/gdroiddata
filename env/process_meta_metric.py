#!/usr/bin/env python
import yaml
from numpy import median, std, var
import numpy as np
import json
import time
import re
# import Levenshtein
from pprint import pprint
from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.4f')

def get_app_from_repo(appname):
    with open('metadata/index-v1.json') as f:
        data = json.load(f)
        for app in data['apps']:
            if app['packageName'] == appname:
                return app

def get_all_apps_from_repo():
    with open('metadata/index-v1.json') as f:
        data = json.load(f)
        return data['apps']

def get_all_packages_from_repo():
    with open('metadata/index-v1.json') as f:
        data = json.load(f)
        return data['packages']

# tries to extract locale-codes from the image urls
def get_locale_of_screenshot_url(ss):
    # 1. try match fastlane links
    p = re.compile("metadata/android/([a-zA-Z0-9-_]*)/images/(.*)Screenshots/")
    ret =p.search(ss)
    if ret is not None:
        ret =ret.group(1)
        return ret

    # try fastlane dir with the images dir
    p = re.compile("metadata/android/([a-zA-Z0-9-_]*)/(.*)Screenshots/")
    ret =p.search(ss)
    if ret is not None:
        ret =ret.group(1)
        return ret

    #2. try metadata links without theandroid dir
    p = re.compile("metadata/([a-zA-Z0-9-_]*)/images/(.*)Screenshots/")
    ret =p.search(ss)
    if ret is not None:
        ret =ret.group(1)
        return ret

    #3. try wrong metadata links without the images directory
    p = re.compile("metadata/([a-zA-Z0-9-_]*)/(.*)Screenshots/")
    ret =p.search(ss)
    if ret is not None:
        ret =ret.group(1)
        return ret

    #4. try the play-syntax
    p = re.compile("play/(.*)/listing/(.*)Screenshots/")
    ret =p.search(ss)
    if ret is not None:
        ret =ret.group(1)
        return ret

    # play syntax without listing dir
    p = re.compile("play/(.*)/(.*)Screenshots/")
    ret =p.search(ss)
    if ret is not None:
        ret =ret.group(1)
        return ret

    # play syntax with code in suffix
    p = re.compile("metadata/play/screenshots/(.*)/")
    ret =p.search(ss)
    if ret is not None:
        ret =ret.group(1)
        return ret

    # publish syntax
    p = re.compile("publish/screenshots/(.*)/")
    ret =p.search(ss)
    if ret is not None:
        ret =ret.group(1)
        return ret

    return 'en' # the language of the repo is returned as default value


if __name__ == '__main__':

    final={}

    # init final result with age of the app
    all_apps = get_all_apps_from_repo()
    for app in all_apps:
        # initialise and add age of app
        final[app['packageName']]={}
        final[app['packageName']]['metrics']={}
        m = final[app['packageName']]['metrics']
        age_in_days = app['lastUpdated']
        now_day = int(round(time.time() * 1000)) /1000 /60 /60 /24
        added_day = int(app['added']) /1000 /60 /60 /24
        updated_day = int(app['lastUpdated']) /1000 /60 /60 /24
        age_day = now_day - added_day
        uptodatesince_day = now_day - updated_day
        m['age_in_days']=age_day # age of firs version
        m['added_on_day']=added_day
        m['uptodatesince']=uptodatesince_day # age of last version
        m['updated_on_day']=updated_day

        # normalize up-to-date-ness ti different amount of months
        utdmonths=[3,6,12,24]
        for utdmonth in utdmonths:
            #utdmonth=3
            utddays=utdmonth*30
            top_value=utddays
            lnorm = (top_value - m['uptodatesince']) / float(top_value)
            if lnorm >= 1.0:
                lnorm = 1.0
            if lnorm <= 0.0:
                lnorm = 0.0
            m['age_last_v_'+str(utdmonth)]=lnorm

    # calculate average timespan between last 3 releases (only if there are 3)
    for appname,packs in get_all_packages_from_repo().items():
        #packs = get_all_packages_from_repo()[app['packageName']]
        if (len(packs) >=3):
            a0 = packs[0]['added']
            a1 = packs[1]['added']
            a2 = packs[2]['added']
            d1 = a0-a1
            d2 = a1-a2
            lavg = (d1 + d2) / float(2)
            avg_days = lavg /1000 /60 /60 /24
            #m['avg_update_frequency'] = avg_days
            final[appname]['metrics']['avg_update_frequency']=avg_days

    # TODO normalize avg_update_frequency like uptodatesince (4 ways)



    # get stats about stars_per_day
    all_vals=[]
    for app in all_apps:
        m = final[app['packageName']]['metrics']
        if 'avg_update_frequency' in m:
            v = m['avg_update_frequency']
            if v>0:
                all_vals.append(v)

    print "\n avg_update_frequency \n"
    print 'max: ', max(all_vals)
    print 'avg: ',sum(all_vals) / float(len(all_vals))
    print 'median: ', median(all_vals)
    print 'stddev: ', std(all_vals, axis=0)
    print 'variance: ', var(all_vals, axis=0)
    
    # lowest value needed here
    top_2_idx = np.argsort(all_vals)[::-1][-(len(all_vals)/5):]
    top_2_values = [all_vals[i] for i in top_2_idx]
    #print top_2_values
    top_value = top_2_values[0]
    print 'top_value: ', top_value

    # normalize avg_update_frequency
    for app in all_apps:
        m = final[app['packageName']]['metrics']
        if 'avg_update_frequency' in m:
            udf = m['avg_update_frequency']
            if udf <= top_value:
                m['avg_update_frequency_normalised'] = 1
            else:
                m['avg_update_frequency_normalised'] = (365-udf)/365
            if m['avg_update_frequency_normalised'] <0:
                m['avg_update_frequency_normalised'] = 0





    # add yaml results
    for source in ['github','gitlab']:
        stream = open('metadata/m_'+source+'_stars.yaml', "r")
        docs = yaml.load_all(stream)
        all_vals=[]
        for doc in docs:
            for k,v in doc.items():
                if isinstance(v, int):
                    # print "found ", k, "->", v
                    all_vals.append(v)

                    # if app was moved to archive, ignore this for now
                    if not k in final:
                        continue

                    # store value in final json
                    app = final[k]
                    app['metrics']['m_'+source+'_stars'] = v
                    stars_per_day= float(app['metrics']['m_'+source+'_stars']) / (float(app['metrics']['age_in_days'])+0.0000001)
                    app['metrics']['m_'+source+'_stars_per_day'] = stars_per_day
            print "\n",

        print source + " stars \n"
        print 'max: ', max(all_vals)
        print 'avg: ',sum(all_vals) / float(len(all_vals))
        print 'median: ', median(all_vals)
        print 'stddev: ', std(all_vals, axis=0)
        print 'variance: ', var(all_vals, axis=0)

        top_2_idx = np.argsort(all_vals)[-(len(all_vals)/5):]
        top_2_values = [all_vals[i] for i in top_2_idx]
        top_value = top_2_values[0]
        print 'top_value: ', top_value

        # normalize stars
        for app in all_apps:
            m = final[app['packageName']]['metrics']
            if 'm_'+source+'_stars' in m:
                stars = m['m_'+source+'_stars']
                if stars >= top_value:
                    m['m_'+source+'_stars_normalised'] = 1
                else:
                    m['m_'+source+'_stars_normalised'] = stars/top_value

        # get stats about stars_per_day
        all_vals=[]
        for app in all_apps:
            m = final[app['packageName']]['metrics']
            if 'm_'+source+'_stars_per_day' in m:
                v = m['m_'+source+'_stars_per_day']
                all_vals.append(v)

        print "\n"+source + " stars_per_day \n"
        print 'max: ', max(all_vals)
        print 'avg: ',sum(all_vals) / float(len(all_vals))
        print 'median: ', median(all_vals)
        print 'stddev: ', std(all_vals, axis=0)
        print 'variance: ', var(all_vals, axis=0)
        
        top_2_idx = np.argsort(all_vals)[-(len(all_vals)/5):]
        top_2_values = [all_vals[i] for i in top_2_idx]
        top_value = top_2_values[0]
        print 'top_value: ', top_value

        # normalize stars per day
        for app in all_apps:
            m = final[app['packageName']]['metrics']
            if 'm_'+source+'_stars_per_day' in m:
                stars = m['m_'+source+'_stars_per_day']
                if stars >= top_value:
                    m['m_'+source+'_stars_per_day_normalised'] = 1
                else:
                    m['m_'+source+'_stars_per_day_normalised'] = stars/top_value
                # print 'DEBUG ', stars , ' top ', top_value , ' norm ' , m['m_'+source+'_stars_per_day_normalised']

        ### end of '+source+' stars

    # add levenshtein distances
    # assemble a list of all package names
    #all_pkg_names=[]
    for app in all_apps:
        pkg_name = app['packageName']
    #    all_pkg_names.append (pkg_name)
        final[pkg_name]['neighbours']={}


    #compare each with all
    # for app in all_apps:
    #     pkg_name = app['packageName']
    #     name = app['name']
    #     distances = {}
    #     for other_app in all_apps:
    #         other_pkg_name = other_app['packageName']
    #         other_name = other_app['name']
    #         if name != other_name:
    #             d = Levenshtein.ratio(name, other_name)
    #             distances[other_pkg_name] = d
    #             #print d
    #     neighbourhood=[]
    #     i=0
    #     for other_pkg_name in sorted(distances, key=distances.get, reverse=True):
    #         neighbourhood.append (other_pkg_name)
    #         i+=1
    #         if i >=3: # more than 3 neighbours doesn't seem to be useful
    #             break
    #     #print (neighbourhood)
    #     final[pkg_name]['neighbours']['l']=neighbourhood


    print "tags "
    # add tags from yaml file
    stream = open('metadata/tags.yaml', "r")
    docs = yaml.load_all(stream)
    for doc in docs:
        #print "d1 "
        #pprint (doc)
        for tag,apps in doc.items():
            #if isinstance(v, int):
            print "tag ", tag, "->", apps
            for app in apps:
                if app in final: # archived apps are not in here any more but can still be tagged
                    if 'tags' not in final[app]:
                        final[app]['tags']=[]
                    final[app]['tags'].append(tag)

    print "tags end"

    # add validated externally available screenshots to G-Droid
    with open('metadata/m_screenshots.json') as f:
        data = json.load(f)
        #pprint (data)
        for app in data: # ssl = screen-shot-list
            ssl = data[app]
            if len(ssl) > 0:
                if app not in final:
                    final[app] = {}
                final[app]['localized']={}
                for ss in ssl:
                    locale = get_locale_of_screenshot_url(ss)
                    #print 'found locale {} for ss {}'.format(locale,ss)
                    if locale not in final[app]['localized']:
                        final[app]['localized'][locale]={}
                        final[app]['localized'][locale]['phoneScreenshots']=[]
                    final[app]['localized'][locale]['phoneScreenshots'].append(ss)

    print "similar apps "
    # add tags from yaml file
    stream = open('metadata/app_match.yaml', "r")
    docs = yaml.load_all(stream)
    for doc in docs:
        #print "d1 "
        #pprint (doc)
        for mainapp,apps in doc.items():
            #if isinstance(v, int):
            print "app ", mainapp, "->", apps
            if mainapp in final:
                neighbourhood=[]
                for app in apps:
                    if mainapp != app: # archived apps are not in here any more
                        neighbourhood.append (app)
                final[mainapp]['neighbours']['a']=neighbourhood


    print "similar apps end"



        
    # print json.dumps(final,indent=1,sort_keys=True)
    with open('metadata/gdroid.json', 'w') as outfile:
        json.dump(final, outfile,indent=1,sort_keys=True)
